#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
// lru 函数声明
// 输入参数描述：
//   pageframeNum：操作系统分配给某进程的页框数目；
//   pageCallSequence：页面调用序列，序列中的每一项是被调用页面的页面号。
////////////////////////////////////////////////////////////////////////////////
void lru(int pageframeNum, vector<int> &pageCallSequence);

int main()
{
    int i, pageframeNum, n;

    cin>>pageframeNum;  // 输入分配给某进程的页框数目(教材上称“页框”为：物理块)
    cin>>n; // 输入该进程的页面调用序列的长度
    vector<int> pageCallSequence(n);    // 定义页面调用序列，序列中的每一项是被调用页面的页面号
    for(i = 0; i < n; i++)  // 输入 n 个页面号，构建页面调用序列
    {
        cin>>pageCallSequence[i];
    }

    lru(pageframeNum, pageCallSequence);    // 模拟最近最久未使用页面置换算法

    return 0;
}
void lru(int pageframeNum, vector<int> &pageCallSequence){  //最近最久未使用页面置换函数
    int miss_num=0,pos=0;   //缺页数，最近最久未使用页面下标
    bool tag;   //缺页标志
    int* v=new int[pageframeNum];   //模拟页框
    int* p=new int[pageframeNum](); //为页框中每个页面设置一个未使用时间
    for(int i=0;i<pageframeNum;i++){
        v[i]=-1;    //将页框中的元素置为-1
    }
    for(unsigned int i=0;i<pageCallSequence.size();i++){
        tag=true;           //标志为true，说明发生缺页
        for(int j=0;j<pageframeNum;j++){
            if(pageCallSequence[i]==v[j]){
                tag=false;  //标志为false，说明未发生缺页
                p[j]=0;     //重置该页未使用时间为0
                continue;
            }
            p[j]+=1;        //其它页未使用时间加1
        }
        if(tag){
            v[pos]=pageCallSequence[i]; //页面置换
            p[pos]=0;                   //设置新页未使用时间为0
            miss_num+=1;                //缺页数加1
        }
        for(int j=0;j<pageframeNum;j++){//重新计算最近最久未使用页面下标
            if(p[pos]<p[j])
                pos=j;
        }
        for(int j=0;j<pageframeNum-1;j++){  //输出页框中的元素
            cout<<v[j]<<",";
        }
        cout<<v[pageframeNum-1]<<endl;      //输出页框中的元素
    }
    cout.setf(ios::fixed);      //不将小数后面的0去掉
    cout<<fixed<<setprecision(3)<<miss_num/(float)pageCallSequence.size();  //保留三位小数输出缺页率
}
